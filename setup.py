import os
import subprocess
import pkg_resources
import setuptools
from distutils.command.install import install

## SETUP
if __name__ == "__main__":
    setuptools.setup(
        name="sample_service",
        version="0.0.1",
        author="Some Rando",
        author_email="rando@blah.com",
        description="A minimal working GRPC service starter template",
        long_description="TODO: pull in README.md",
        long_description_content_type="text/markdown",
        url="https://your-git-repo.com/you/project",
        project_urls={
            "Bug Tracker": "https://jira.company.com",
        },
        classifiers=[
            "Programming Language :: Python :: 3"
        ],
        packages=setuptools.find_packages(include=['.*']),
        package_dir={"": "."},
        python_requires=">=3.7"
    )