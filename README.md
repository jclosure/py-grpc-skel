# Sample minimal python GRPC starter app

## Installation
```shell
make install
```

## Demo usage

Open 2 terminals
- term1 = server
- term2 = client

### Run the server in term1
```shell
make run-server
```

### Run the client in term2
```shell
make run-client
```

## Notables

### Protos
See `./protobufs/*.proto`
```
service Sample {
  rpc MakeRequest (Request) returns (Response);
  rpc StreamRequests (stream Request) returns (Response);
  rpc StreamResponses (Request) returns (stream Response);
  rpc StreamBidi (stream Request) returns (stream Response);
}
```
See `make protos`

### Implementation
See `SampleService` in `./sample/sample_server.py`

