from __future__ import print_function
import logging
import grpc

from api.v1 import sample_service_pb2
from api.v1 import sample_service_pb2_grpc

HOST = "localhost"
PORT = 8081

def run():
    server_host = f'{HOST}:{PORT}'

    with grpc.insecure_channel(server_host) as channel:
        stub = sample_service_pb2_grpc.SampleStub(channel)
        print("Server connected: {}".format(server_host))
        
        request = sample_service_pb2.Request(
            data = "Hello"
        )

        # MAKE A STANDARD REQ/RESP
        response = stub.MakeRequest(request)
        print(f"Client request: {request}")
        print(f"Client received: {response}")

        # MAKE A REQ AND STREAM BACK RESPONSES
        response_iterator = stub.StreamResponses(request)
        for response in response_iterator:
            print(f"Client request: {request}")
            print(f"Client received: {response}") 

        # STREAM REQUESTS TO SERVER AND RECEIVE A SINGLE RESPONSE
        dataset = ["foo", "bar"]
        request_iterator = iter([sample_service_pb2.Request(data=x) for x in dataset])
        response = stub.StreamRequests(request_iterator)
        print(f"Client received: {response}") 


        # STREAM REQUESTS AND RESPONSES BIDIRECTIONALLY
        dataset = ["foo", "bar"]
        request_iterator = iter([sample_service_pb2.Request(data=x) for x in dataset])
        response = stub.StreamBidi(request_iterator)
        print(f"Client received: {response}") 

if __name__ == '__main__':
    run()
