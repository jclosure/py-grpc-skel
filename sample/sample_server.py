from concurrent import futures
import grpc

from api.v1 import sample_service_pb2
from api.v1 import sample_service_pb2_grpc

PORT = 8081
MAX_WORKERS = 10

class SampleService(sample_service_pb2_grpc.SampleServicer):

    def MakeRequest(self, request, context):
        """
        Simple request/response
        """
        print(f'received request: {request}')
        
        # TODO: processing

        # form response
        response = sample_service_pb2.Response(
            result = "OK"
        )

        print(f'returning response: {response}')
        return response


    def StreamRequests(self, request_iterator, context):
        """
        Stream multiple requests, return a single response
        """
        print(f'received request_iterator: {request_iterator}')
        for request in request_iterator:
            print(f'processing request: {request}')

        # form response
        response = sample_service_pb2.Response(
            result = "OK"
        )

        print(f'returning response: {response}')
        return response


    def StreamResponses(self, request, context):
        """
        Single request, stream back multple responses.
        """
        print(f'received request: {request}')

        for i in range(10):
            response = sample_service_pb2.Response(
                result = "OK"
            )
            print(f'returning response: {response}')
            yield response


    def StreamBidi(self, request_iterator, context):
        """
        Bidirectional streaming of requests and responses
        """
        print(f'received request_iterator: {request_iterator}')
        for i, request in enumerate(request_iterator):
            print(f'processing request: {request} - {i}')
            if i % 2 == 0:
                response = sample_service_pb2.Response(
                    result = "OK"
                )
                print(f'returning response: {response}')
                yield response

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=MAX_WORKERS))
    sample_service_pb2_grpc.add_SampleServicer_to_server(SampleService(), server)
    server.add_insecure_port(f'[::]:{PORT}')
    server.start()
    try:
        server.wait_for_termination()
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
