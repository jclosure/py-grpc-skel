
install: protos
	pip3 install -e .

uninstall:
	pip3 uninstall sample-service

protos: ## build the protos
	mkdir -p ./sample/api/v1 \
	&& python3 -m grpc_tools.protoc -Iprotobufs --grpc_python_out=./sample/api/v1 --python_out=./sample/api/v1 protobufs/*.proto

clean:
	(rm -rfv build **/build ./thirdparty/install **/*.pyc)


## Developer productivity targets

run-server:
	PYTHONPATH="$$(pwd)/sample/api/v1:PYTHONPATH" python3 ./sample/sample_server.py

run-client:
	PYTHONPATH="$$(pwd)/sample/api/v1:PYTHONPATH" python3 ./sample/sample_client.py




